# syntax=docker/dockerfile:1
FROM python

WORKDIR /app
COPY . .

RUN pip3 install -r requirements.txt

# RUN yarn install --production

CMD ["python3", "-m", "coltec"]
EXPOSE 5000
