from flask import Blueprint, render_template

from coltec.models import db

articles_ctrl = Blueprint('artigos', __name__)

@articles_ctrl.route('/')
def index():
    return render_template('page_index.html')
