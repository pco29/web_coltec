from coltec import app, db
from coltec.class_articles.main import articles_ctrl
# from coltec.models import Video
import datetime
from flask import Flask, render_template, redirect
from sqlalchemy  import  extract
import requests
from markdown2 import Markdown
import os

db.create_all()

app.register_blueprint(articles_ctrl, url_prefix='/artigos')

@app.route('/')
def show_all():
    return render_template('page_index.html')

@app.route('/artigos')
def show_news():
    return render_template('page_blank.html')

@app.route('/estatuto')
def show_estatuto():
    estatuto = requests.get("https://gitlab.com/pco29/secretaria/-/raw/main/estatuto/readme.md")
    estatuto_html = Markdown().convert( estatuto.content.decode("utf-8") )
    return render_template('page_blank.html', body=estatuto_html )

@app.route('/programa')
def show_programa():
    request = requests.get("https://gitlab.com/pco29/secretaria/-/raw/main/programa/readme.md")
    programa_html = Markdown().convert( request.content.decode("utf-8") )
    return render_template('page_blank.html', body=programa_html )

@app.route('/sobre')
def show_sobre():
    return render_template('page_blank.html', content="opaaaaa")

@app.route('/update')
def update():
    os.system("git pull origin main")
    return redirect("/")


app.run(debug=True, host="0.0.0.0")
