from coltec import db
from datetime import datetime



class Video(db.Model):
    __tablename__ = 'videos'
    iid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(512))
    youtube_id = db.Column(db.String(32))
    date = db.Column(db.DateTime)

    def __init__(self, title, youtube_id, date):
        self.title = title
        self.youtube_id = youtube_id
        self.date = date
